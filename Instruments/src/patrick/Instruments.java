package patrick;  //The 'package' is arranging the program for use later.  "Good Housekeeping" essentially.

public class Instruments { 	//This bracket { is the opening of the class 'Instruments.java'.  The ending of the class is the last } bracket.  Notice that inside of these 
							//brackets there are more {} brackets.  These other brackets are the start and end of each method.
	
	//'public class' is defining this specific class as public.  Because it is public, it can be accessed from anywhere in the program .
	//The class can be called anything.  In this case it has been called 'Instruments'
	
  public static void main(String args[]){  	//As described on line 3 and 4, this bracket is the opening of a method.  Inside these brackets everything the method does
	  										//is defined.
	  
	  //This a "standard" main method that is always called first from the runtime environment.  Without this the program will not run.  Think of it as
	  //the ignition on a car.  It has to happen first before everything else can run.
  
	Guitar myFirstGuitar = new Guitar ();
	
	//This is a type of class that has been defined.  It was written in the class 'Guitar.java' in the other tab.  The class 'Guitar.java' holds all the attributes that 
	//every instance can have.  The instance has been named myFirstGuitar.  Later, another instance of type Guitar (this is the class) can be created and named differently
	//e.g. mySecondGuitar.  It will have all of the attributes defined in Guitar.java, and each attribute can have different values.  These values will be set in the same
	//way as below.
	 
	  myFirstGuitar.noStrings = 4 ;
	  
	  //In this line, the value of the attribute 'noStrings' is being set for the instance of type Guitar named myFirstGuitar.  The instance has been created on line 13.
	  //The attribute has been defined in the class file Guitar.java on the next tab.
	  
	  myFirstGuitar.noPickups = 3;
	  
	  //In this line, the value of the attribute 'noPickups' is being set for the instance of type Guitar named myFirstGuitar.  The instance has been created on line 13.
	  //The attribute has been defined in the class file Guitar.java on the next tab.
	  
	  myFirstGuitar.frets = 20;
	  
	  //In this line, the value of the attribute 'frets' is being set for the instance of type Guitar named myFirstGuitar.  The instance has been created on line 13.
	  //The attribute has been defined in the class file Guitar.java on the next tab.
	  
	  myFirstGuitar.colour = "Black";
	  
	  //In this line, the value of the attribute 'colour' is being set for the instance of type Guitar named myFirstGuitar.  The instance has been created on line 13.
	  //The attribute has been defined in the class file Guitar.java on the next tab.

	  
     System.out.println("Your guitar has " + myFirstGuitar.noStrings + " strings");
     
     //The command 'System.out.println' prints/displays the text inside the quotation marks which must be inside the brackets, along with the value of the 
     //attribute 'noStrings' of the instance of 'Guitar' named 'myFirstGuitar' (line 13) which is written as myFirstGuitar.noStrings
          
     System.out.println("Your guitar has " + myFirstGuitar.noPickups + " pickups");
     
     //The command 'System.out.println' prints/displays the text inside the quotation marks which must be inside the brackets, along with the value of the 
     //attribute 'noPickups' of the instance of 'Guitar' named 'myFirstGuitar' (line 13) which is written as myFirstGuitar.noPickups
     
     System.out.println("Your guitar has " + myFirstGuitar.frets + " frets");
     
     //The command 'System.out.println' prints/displays the text inside the quotation marks which must be inside the brackets, along with the value of the 
     //attribute 'frets' of the instance of 'Guitar' named 'myFirstGuitar' (line 13) which is written as myFirstGuitar.frets
     
     System.out.println("Your guitar is " + myFirstGuitar.colour);
     
     //The command 'System.out.println' prints/displays the text inside the quotation marks which must be inside the brackets, along with the value of the 
     //attribute 'colour' of the instance of 'Guitar' named 'myFirstGuitar' (line 13) which is written as myFirstGuitar.colour
  }
}
